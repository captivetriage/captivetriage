# What is it?

Because of the **covid-19 situation** we created an open source **lowteck** standalone tool for healthcare systems in other to help with the preliminary initial patient triage.
It consists in a **captive hotspot portal** witch provides a SPA user interface with a very simple and customizable structured triage system.
We hope that this can **help in manages people in stress situations of hospital admission**.

# What do I need?

It does **not requires any special hardware installation**. You only need raw raspberryPi with wifi capabilities like raspberryPi3. And an SD > 500mb to setup the application.
Requeriments list:

- RaspberryPi3 (Or equivalent)
- SD cart
- Windows 10 (OPTIONAL To use wizard setup)

# How does it works?

The captive network provides client users (like patient's mobiles) with an interface based in a quick test with a workflow with some cardinal information layers and one result screen: 

*(Language) -> (Condicion) -> (Symptoms) -> (Result Information)*

Every step based on this schematic can be totally customizable. This is, you can add languages, patient relevant symptoms or comorbidity options. 
All this inputs can be scored in other to provide result information relevant for the user, like a cue number, location indications or other critical information.
In addition all this data is saved in the local server and can be consulted via a standard endpoint, you can easily integrate this preliminary triage with your current triage or administrative software.

# How to setup it?

This software comes with a default setup in other to help with the **COVID-19 triage**. 

This includes:
  1. Languages:
     - English
     - Spanish
  2. Condition:
     - Age
     - Diabetes flag
     - Hipertension flag
  3. Symtomps:
     - Fever, (day's duration popup)
     - Cough, (day's duration popup)
     - Respiratory distress, (day's duration popup)
     - Other symtomps
       - I think I have covid19 becouse (Custom text)
       - Medical emergency (heart attack, stroke, etc ...)
       - Other affection (Custom text)
  4. Result information:
     - Instruction set 1
     - Instruction set 2

Using the setup wizard you can easily edit this workflow or create your own.

# Download Releases

Go to *releases folder*

# Want to help?

Want to fix a bug, contribute with some code or new ideas? **Excellent you are a hero**! Enter to our **discord channel** and talk with some administrator:
